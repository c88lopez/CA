ej1 :: ([a], [Int]) -> [a]
ej1 (_, []) = []
ej1 (elementos, posicion:resto) =
    itemPorPosicion(posicion, elementos) : ej1 (elementos, resto)

itemPorPosicion :: (Int, [a]) -> a
itemPorPosicion (1, x:_) = x
itemPorPosicion (i, x:y) = itemPorPosicion (i-1, y)

ej2 :: (Int, Int, Int) -> [[Int]]
ej2 (0, desde, hasta) = [[]]
ej2 (cantidad, desde, hasta) = 
    if desde < hasta
        then repetirNumero(cantidad, desde) : ej2(cantidad, desde+1, hasta)
        else repetirNumero(cantidad, hasta) : []

repetirNumero :: (Int, Int) -> [Int]
repetirNumero (0, _) = []
repetirNumero (cantidad, numero) = numero : repetirNumero(cantidad-1, numero)

-- ej3 :: ([[a]], [[a]], Int) -> [[a]]
-- ej3
