ej1 :: ([Int], Int) -> Int
ej1 ([], _) = 0
ej1 (nPar:(nImpar:resto), aguja) =
    if nPar == aguja
        then 1 + ej1(resto, aguja)
        else ej1(resto, aguja)

ej2 :: [String] -> [String]
ej2 [] = []
ej2 (palabra:resto) =
    if palabraOrdenada(palabra)
        then palabra : ej2(resto)
        else ej2(resto)

palabraOrdenada :: String -> Bool
palabraOrdenada (letra1:(letra2:[])) = letra1 < letra2
palabraOrdenada (letra1:(letra2:resto)) =
    if letra1 < letra2
        then palabraOrdenada(letra2:resto)
        else False

ej3 :: [[Int]] -> Int
ej3 z = mayorEnLista(
    primerRepetido(
        traspuesta(
            z
        )
    ))

primerRepetido :: Eq a => [[a]] -> [a]
primerRepetido [] = []
primerRepetido (lista:resto) = 
    if existeLista(lista, resto)
        then lista
        else primerRepetido(resto)

existeLista :: Eq a => ([a], [[a]]) -> Bool
existeLista (_, []) = False
existeLista (aguja, (lista:resto)) =
    if listasIguales(aguja, lista)
        then True
        else existeLista(aguja, resto)

listasIguales :: Eq a => ([a], [a]) -> Bool
listasIguales ([], []) = True
listasIguales ([], _) = False
listasIguales (_, []) = False
listasIguales ((x:y), (u:v)) =
    if x == u
        then listasIguales(y, v)
        else False

traspuesta :: [[a]] -> [[a]]
traspuesta ([]:_) = []
traspuesta z = todasLasCabezas(z) : traspuesta(todasLasColas(z))

todasLasCabezas :: [[a]] -> [a]
todasLasCabezas [] = []
todasLasCabezas ((x:_):z) = x : todasLasCabezas(z)

todasLasColas :: [[a]] -> [[a]]
todasLasColas [] = []
todasLasColas ((_:y):z) = y : todasLasColas(z)

mayorEnLista :: [Int] -> Int
mayorEnLista (x:[]) = x
mayorEnLista (x:(y:[])) = if x > y then x else y
mayorEnLista (x:(y:resto)) =
    if x > y
        then mayorEnLista(x:resto)
        else mayorEnLista(y:resto)