module GuiaEj1Complementarios where

-- Ej adicional, maximo comun divisor
myMaxComDiv :: (Int, Int) -> Int
myMaxComDiv (m, n) = if (m==n) 
    then m 
    else if (m>n) 
        then myMaxComDiv(m-n, n) 
        else myMaxComDiv(m, n-m)

myMenoresOIg :: ([Int], Int) -> [Int]
myMenoresOIg ([], _) = []
myMenoresOIg (x:y, e) = 
    if x <= e
        then x:myMenoresOIg(y, e)
        else myMenoresOIg(y, e)

myMayores :: ([Int], Int) -> [Int]
myMayores ([], _) = []
myMayores (x:y, e) = 
    if x > e
        then x:myMayores(y, e)
        else myMayores(y, e)

myQuickSort :: [Int] -> [Int]
myQuickSort [] = []
myQuickSort (x:y) = 
    myQuickSort(
        myMenoresOIg(y, x)
    )   
    ++
    x:myQuickSort(
        myMayores(y, x)
    )
    
-- palabrasSinVocales :: String -> [String]

-- "" => [""]
-- " vez" => ["", "vez"]
-- "habia una vez" => ["habia", "una", "vez"]
--- [""]           [[]]
--- ["h"]          [['h']]
--- ["ha"]         [['h', 'a']]
--- ["hab"]        [['h', 'a', 'b']]
--- ["habi"]       [['h', 'a', 'b', 'i']]
--- ["habia"]      [['h', 'a', 'b', 'i', 'a']]
--- ["habia", ""]  [['h', 'a', 'b','i', 'a'], []]
--- ["habia", "u"] [['h', 'a', 'b','i', 'a'], ['u']]
mySplit :: (String, Char, [String]) -> [String]
mySplit ("", _, result) = result
-- mySplit ((letra : resto), delim, lista) = 
--     if letra == delim
--         then mySplit(, delim, )
--         else mySplit(resto, delim, result)

agregarAlFinal :: ([[a]], a) -> [[a]]
agregarAlFinal ([], x) = [[x]]
agregarAlFinal (ultimo : [], x) = agregarALista(ultimo, x)
agregarAlFinal (noUltimo : resto, x) = noUltimo : agregarAlFinal(resto, x)

agregarALista :: ([a], a) -> [a]
agregarALista ([], x) = [x]
agregarALista (item : resto, x) = item : agregarALista (resto, x)

tieneVocales :: String -> Bool
tieneVocales [] = False
tieneVocales (letra:resto) = if esVocal(letra)
    then True
    else tieneVocales(resto)

esVocal :: Char -> Bool
esVocal 'a' = True
esVocal 'e' = True
esVocal 'i' = True
esVocal 'o' = True
esVocal 'u' = True
esVocal _ = False

traspuesta :: [[a]] -> [[a]]
traspuesta ([]:_) = []
traspuesta z = todasLasCabezas(z) : traspuesta(todasLasColas(z))

todasLasCabezas :: [[a]] -> [a]
todasLasCabezas [] = []
todasLasCabezas ((x:_):z) = x : todasLasCabezas(z)

todasLasColas :: [[a]] -> [[a]]
todasLasColas [] = []
todasLasColas ((_:y):z) = y : todasLasColas(z)