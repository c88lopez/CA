myFilter :: ([a], a -> Bool) -> [a]
myFilter ([], _) = []
myFilter ((x:y), f) = if f x
    then x:myFilter (y, f)
    else myFilter (y, f)

esPositivo :: Int -> Bool
esPositivo x = x >= 0

myMap :: ([a], a -> b) -> [b]
myMap ([], _) = []
myMap ((x:y), f) = 
    (f x) : myMap (y, f)

siguienteNumero :: Int -> Int
siguienteNumero x = x+1