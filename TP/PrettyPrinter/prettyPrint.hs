{- 

Fecha de entrega: 16-05-2018
Autor: Cristian Lopez
 
Curso: Computación Avanzada a cargo de Ariel Arbiser
Trabajo Práctico: Pretty Printer sobre el lenguaje de programación `golang`

Pequeño manual de uso:
al cargar el actual archivo, simplemente escribimos el comando `prettyPrint` y le damos enter. Esto generará que el 
programa se quede esperando la siguiente linea, que será el código a formatear.

Ejemplos de respuesta por orden de complejidad:
1. 
input:

package main;import "fmt";func main() {fmt.Println("Hello Wolrd!");}

output:

package main;
import "fmt";
func main() {
    fmt.Println("Hello Wolrd!");
    
}

2. 
input:  

package main;import "fmt";func main() {if 7%2 == 0 {fmt.Println("7 is even")} else {fmt.Println("7 is odd")} }

output:

package main;
import "fmt";
func main() {
    if 7%2 == 0 {
        fmt.Println("7 is even")
    } else {
        fmt.Println("7 is odd")
    } 
}

3. 
input:  

package main;import "fmt";func main() {i := 1; for i <= 3 {fmt.Println(i); i = i + 1 } }

output:

package main;
import "fmt";
func main() {
    i := 1;
     for i <= 3 {
        fmt.Println(i);
         i = i + 1 
    } 
}

4. 
input:  

package main;import "fmt";func main() {for _, i := range []int{7, 42} {if r, e := f1(i); e != nil {fmt.Println("f1 failed:", e);} else {fmt.Println("f1 worked:", r)}} }

output:

package main;
import "fmt";
func main() {
    for _, i := range []int{
        7, 42
    } {
        if r, e := f1(i);
         e != nil {
            fmt.Println("f1 failed:", e);

        } else {
            fmt.Println("f1 worked:", r);

        }
    } 
}

-}

import System.IO

{-
Función principal de entrada. Al ejecutarla, estará esperando el contenido en la
siguiente linea, luego devolverá su resultado.
-}
prettyPrint :: IO()
prettyPrint = do
    code <- getLine
    putStrLn(
        myConcat(
            "==================================\n", 
            parse(code, 0)
        ))

{- 
Esta es la funcion principal del TP recibirá el string en la forma (primer caracter:resto string).
En base al primer caracter, tomará una decisión.
-}
parse :: (String, Int) -> String
parse ("", _) = ""
parse ((char:leftCode), deep) = 
    if charWhichIndents(char)
        then 
            if startsBlock(char)
                then -- We have a `{`
                    myConcat(
                        char : "\n", 
                        myConcat(
                            getSpaces(spacesPerLevel(deep + 1)),
                            parse(leftCode, deep + 1)
                        )
                    )
                else 
                    if endsBlock(char)
                        then -- We have a `}`
                            myConcat(
                                "\n",
                                myConcat(
                                    getSpaces(spacesPerLevel(deep - 1)),
                                    myConcat(
                                        [char],
                                        parse(leftCode, deep - 1) 
                                    )
                                )
                            )
                        else -- We have a `;`
                            myConcat(
                                char : "\n",
                                myConcat(
                                    getSpaces(spacesPerLevel(deep)),
                                    parse(leftCode, deep)
                                )
                            )
        else char:parse(leftCode, deep)

{-
Define cuales son los caracteres que generan un cambio de identación.
-}
charWhichIndents :: Char -> Bool
charWhichIndents '{' = True
charWhichIndents '}' = True
charWhichIndents ';' = True
charWhichIndents _ = False

{-
Caracteres que abren un bloque.
-}
startsBlock :: Char -> Bool
startsBlock '{' = True
startsBlock _ = False

{-
Caracteres que cierran un bloque.
-}
endsBlock :: Char -> Bool
endsBlock '}' = True
endsBlock _ = False

{-
Devuelve la cantidad de espacios pedidos
-}
getSpaces :: Int -> String
getSpaces 0 = ""
getSpaces count = myConcat(" ", getSpaces(count - 1))

{- 
Se define la cantidad de espacios por nivel. Entendiendose nivel por la "profundidad" o anidación del bloque.
Por ejemplo:
func main() { // la función `main` está definida en el nivel 0 del código.
    fmt.Println("Hello Wolrd!"); // el contenido de dicha función
}
-}
spacesPerLevel :: Int -> Int
spacesPerLevel x = x * 4

{-
myConcat tiene el prefijo `my` ya que ya existe concat, de prelude.
-}
myConcat :: ([a], [a]) -> [a]
myConcat (x, []) = x
myConcat ([], y) = y
myConcat ((x:y), v) = x : myConcat(y, v)

{- 
Notas:
1. Se ha utilizado la librería auxiliar IO con el fin de usabilidad con el manejo de comillas. Esto permite no tener
    que preocuparse por escapar las comillas del código a parsear. Las funciones utilizadas de esta librería son:
     a. getLine: Lectura de una línea del standard input (consola en nuestro caso).
     b. putStrLn: Imprime en la standard ouput (consola en nuestro caso) el string argumento, con el salto de linea como
                    sufijo.

Conclusiones:
1. Resulta interesante el uso del Pattern Matching. Esto permitió utilizar el motor de Haskell para definir diccionarios
    (como las funciones charWithIndents, startsBlock, etc.), logrando tener un código más prolijo y de fácil lectura 
    evitando el uso de ifs anidados, concentrandonos en el algorítmo.
2. Comparandolo con el paradigma imperativo, nos ahorramos el uso de variables explotando la composición de funciones.
    Esto resulto desafiante ya que, al estar de moda los lenguajes imperativos, suelen usarse variables como auxiliares 
    (y muchas veces en exceso). Esta caractistica obliga a definir funciones especificas con una clara responsabilidad, 
    logrando la reutilización de las mismas. 
3. A sí mismo, la composición junto con la recursivididad se utiliza como los iteradores (for, while, do-while). Esto 
    se ha logrado definiendo correctamente condiciones base en el pattern matching y con un proposito claro de las
    funciones.

-}